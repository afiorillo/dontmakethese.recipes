
serve:
	hugo server \
	--theme slim \
	--source dontmakethese/ \
	--buildDrafts

public:
	hugo \
	--minify \
	--theme slim \
	--source dontmakethese/ \
	--destination ../public/
